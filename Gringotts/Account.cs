﻿using System;

namespace Gringotts
{
    class Account
    {
        protected decimal _balance;
        private string _userName;
        private int _pin;

        public decimal Balance
        {
            get { return this._balance; }
            set { this._balance = value; }
        }

        public string DisplayName
        {
            get { return this._userName; }
            set
            {
                bool valid = false;
                foreach (char letter in value)
                {
                    if (letter >= 'A' && letter <= 'Z' || letter >= 'a' && letter <= 'z')
                        valid = true;
                    else
                    {
                        valid = false;
                        break;
                    }
                }
                if(valid)
                    this._userName = value;
                else
                    throw new Exception("Invalid username.");
            }
        }

        public int PIN
        {
            get { return this._pin; }
            set
            {
                if (value >= 0 && value.ToString().Length == 6)
                    this._pin = value;
                else
                    Console.WriteLine("Invalid PIN.");
            }
        }
        public void CashIn(decimal amount)
        {
            if (amount > 0)
                this._balance += amount;
            else
                throw new Exception("Invalid Balance.");
        }
    }
}