﻿using System;

namespace Gringotts
{
    class BankAccount : Account
    {
        private string _bankName;

        public string BankName
        {
            get { return this._bankName; }
            set
            {
                bool valid = false;
                foreach (char letter in value)
                {
                    if (letter >= 'A' && letter <= 'Z' || letter >= 'a' && letter <= 'z')
                        valid = true;
                    else
                    {
                        valid = false;
                        break;
                    }
                }
                if (valid)
                    this._bankName = value;
                else
                    throw new Exception("Invalid Bank Name");

            }
        }

        public decimal TrasferMoney(decimal amount, out bool transferedOrNot)
        {
            if(amount <= this.Balance)
            {
                this._balance -= amount;
                transferedOrNot = true;
                return amount;
            }
            Console.WriteLine("Not Enough funds.");
            transferedOrNot = false;
            return 0;
        }
    }
}