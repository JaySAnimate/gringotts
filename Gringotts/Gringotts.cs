﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Gringotts
{
    class GringottsWallet : IWalletActions
    {
        //Accounts
        Wallet ServiceWallet;
        BankAccount BankCard;

        //Tools
        private UtilityPayments[] UtilityFees;
        private List<EntertainmentOrShopping> EntOrShop = new List<EntertainmentOrShopping>();
        public static readonly Dictionary<UtilityServices, decimal> UnitFees;
        public static readonly Dictionary<UtilityServices, int> UnitsSpent;
        
        //Database
        private string UtilityDataFile = string.Format(@"../../data/{0}_UtilityFees", LogIn._activeUser);
        private string ServiceWalletDataFile = string.Format(@"../../data/{0}_ServiceWallet", LogIn._activeUser);
        private string CardDataFile = string.Format(@"../../data/{0}_Card", LogIn._activeUser);
        private string PaidUtilityDataFile = string.Format(@"../../data/{0}_PaidUtilities", LogIn._activeUser);
        private string ShopAndEntDataFile = string.Format(@"../../data/{0}_EntAndShops", LogIn._activeUser);

        //Properties
        public Wallet Wallet
        {
            get { return this.ServiceWallet; }
        }
        public BankAccount Card
        {
            get { return this.BankCard; }
        }

        public UtilityPayments[] UPayments 
        { 
            get { return this.UtilityFees; } 
        }

        //Constructors
        static GringottsWallet()
        {
            UnitFees = new Dictionary<UtilityServices, decimal>();
            UnitsSpent = new Dictionary<UtilityServices, int>();
        }
        public GringottsWallet()
        {
            LoadAccounts();
            LoadUtilityData();
            GenerateUnitFees();
            if(!File.Exists(UtilityDataFile))
                GenerateNewUtilityFees();
        }

        //SignUp Staff
        public bool ConfigureServiceWallet(string displayname, int pin, out string status)
        {
            if(displayname != "" & pin.ToString().Length == 6)
            {
                try
                {
                    this.ServiceWallet = new Wallet();
                    this.ServiceWallet.DisplayName = displayname;
                    this.ServiceWallet.PIN = pin;

                    FileStream ServiceStream = new FileStream(this.ServiceWalletDataFile, FileMode.Create);
                    BinaryWriter WalletDataWritter = new BinaryWriter(ServiceStream);
                    WalletDataWritter.Write(this.ServiceWallet.DisplayName);
                    WalletDataWritter.Write(this.ServiceWallet.PIN);
                    WalletDataWritter.Write(this.ServiceWallet.Balance);
                    ServiceStream.Close();
                    WalletDataWritter.Close();

                    status = "OK";
                    return true;
                }
                catch (IOException e)
                {
                    status = e.Message;
                    return false;
                }
                catch (Exception e)
                {
                    status = e.Message;
                    return false;
                }
            }
            status = "Invalid Display Name or PIN length is not 6.";
            return false;
        }
        public bool AddBankAccount(string bankName, out string status)
        {
            try
            {
                this.BankCard = new BankAccount();
                this.BankCard.BankName = bankName;
                this.BankCard.Balance = 1800m;

                FileStream CardStream = new FileStream(this.CardDataFile, FileMode.Create);
                BinaryWriter CardDataWritter = new BinaryWriter(CardStream);
                CardDataWritter.Write(this.Card.BankName);
                CardDataWritter.Write(this.Card.PIN);
                CardDataWritter.Write(this.Card.Balance);
                CardStream.Close();
                CardDataWritter.Close();

                status = "OK";
                return true;
            }
            catch (IOException e)
            {
                status = e.Message;
                return false;
            }
            catch (Exception e)
            {
                status = e.Message;
                return false;
            }
        }

        //Fees
        private void GenerateUnitFees()
        {
            UnitFees[UtilityServices.Electricity] = 1.38m;
            UnitFees[UtilityServices.Gas] = 0.415m;
            UnitFees[UtilityServices.Internet] = 25m;
            UnitFees[UtilityServices.Phone] = 0.27m;
            UnitFees[UtilityServices.TV] = 100m;
            UnitFees[UtilityServices.Water] = 1.33m;
        }
        private void GenerateUnitsSpent()
        {
            UnitsSpent[UtilityServices.Electricity] = new Random().Next(85, 130);
            UnitsSpent[UtilityServices.Gas] = new Random().Next(150, 210);
            UnitsSpent[UtilityServices.Internet] = 1;
            UnitsSpent[UtilityServices.Phone] = new Random().Next(100, 160);
            UnitsSpent[UtilityServices.TV] = 1;
            UnitsSpent[UtilityServices.Water] = new Random().Next(27, 45);
        }
        public bool GenerateNewUtilityFees()
        {
            GenerateUnitsSpent();
            this.UtilityFees = new UtilityPayments[6];
            for (int i = 0; i < UtilityFees.Length; i++)
                UtilityFees[i] = new UtilityPayments((UtilityServices)(i + 1));
            
            try
            {
                FileStream UtilityDataStream = new FileStream(this.UtilityDataFile, FileMode.Create);
                BinaryWriter BinaryUtilityData = new BinaryWriter(UtilityDataStream);

                for (int i = 0; i < UtilityFees.Length; i++)
                {
                    BinaryUtilityData.Write((int)UtilityFees[i].Service);
                    BinaryUtilityData.Write(UtilityFees[i].PerUnitFee);
                    BinaryUtilityData.Write(UtilityFees[i].UnitsSpent);
                    BinaryUtilityData.Write(UtilityFees[i].UpTo);
                    BinaryUtilityData.Write(UtilityFees[i].Debt);
                    BinaryUtilityData.Write(UtilityFees[i].Paid);
                    BinaryUtilityData.Write(UtilityFees[i].PaymentDate);
                }
                UtilityDataStream.Close();
                BinaryUtilityData.Close();
                return true;
            }
            catch (IOException e) 
            {
                Console.WriteLine(e.Message);
                return false;
            }            
        }

        //Load Staff
        public string LoadAccounts()
        {
            try
            {
                if(File.Exists(this.ServiceWalletDataFile))
                {
                    FileStream ServiceStream = new FileStream(this.ServiceWalletDataFile, FileMode.Open);
                    BinaryReader WalletDataReader = new BinaryReader(ServiceStream);

                    this.ServiceWallet = new Wallet();

                    this.ServiceWallet.DisplayName = WalletDataReader.ReadString();
                    this.ServiceWallet.PIN = WalletDataReader.ReadInt32();
                    this.ServiceWallet.Balance = WalletDataReader.ReadDecimal();
                    ServiceStream.Close();
                    WalletDataReader.Close();
                }
                if (File.Exists(this.CardDataFile))
                {
                    FileStream CardStream = new FileStream(this.CardDataFile, FileMode.Open);
                    BinaryReader CardDataReader = new BinaryReader(CardStream);

                    this.BankCard = new BankAccount();

                    this.Card.BankName = CardDataReader.ReadString();
                    this.Card.PIN = CardDataReader.ReadInt32();
                    this.Card.Balance = CardDataReader.ReadDecimal();
                    CardStream.Close();
                    CardDataReader.Close();
                }
                return "OK";
            }
            catch (IOException e)
            {
                return e.Message;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        public string LoadUtilityData()
        {
            try
            {
                FileStream UtilityDataStream = new FileStream(this.UtilityDataFile, FileMode.Open); 
                BinaryReader BinaryUtilityData = new BinaryReader(UtilityDataStream);

                UtilityFees = new UtilityPayments[6];
                for (int i = 0; i < UtilityFees.Length; i++)
                    UtilityFees[i] = new UtilityPayments();

                for (int i = 0; i < UtilityFees.Length; i++)
                {
                    UtilityFees[i].Service = (UtilityServices)BinaryUtilityData.ReadInt32();
                    UtilityFees[i].PerUnitFee = BinaryUtilityData.ReadDecimal();
                    UtilityFees[i].UnitsSpent = BinaryUtilityData.ReadInt32();
                    UtilityFees[i].UpTo = BinaryUtilityData.ReadString();
                    UtilityFees[i].Debt = BinaryUtilityData.ReadDecimal();
                    UtilityFees[i].Paid = BinaryUtilityData.ReadDecimal();
                    UtilityFees[i].PaymentDate = BinaryUtilityData.ReadString();
                }
                UtilityDataStream.Close();
                BinaryUtilityData.Close();
                return "OK";
            }
            catch (IOException e) 
            {
                return e.Message;
            }
        }       

        //Interface Stuff
        public bool CashIn(decimal amount, out string status)
        {
            try
            {
                this.ServiceWallet.CashIn(amount);
                status = "OK";
                return true;
            }
            catch (Exception e)
            {
                status = e.Message;
                return false;
            }
        }
        public bool Pay(out string status, bool viaCard, params UtilityServices[] services)
        {
            Account CardOrWallet = this.ServiceWallet;
            if (viaCard)
                CardOrWallet = this.BankCard;
            try
            {
                for(int i = 0; i < services.Length; i++)
                {
                    if (services[i] != UtilityServices.None && this.UtilityFees[(int)services[i] - 1].Debt <= CardOrWallet.Balance)
                    {
                        CardOrWallet.Balance -= this.UtilityFees[(int)services[i] - 1].Debt;
                        this.UtilityFees[(int)services[i] - 1].Paid = this.UtilityFees[(int)services[i] - 1].Debt;
                        this.UtilityFees[(int)services[i] - 1].Debt = 0;
                        this.UtilityFees[(int)services[i] - 1].PaymentDate = DateTime.Now.Date.ToString();

                        FileMode mode = FileMode.Create;
                        if (File.Exists(PaidUtilityDataFile))
                            mode = FileMode.Append;

                        FileStream UtilityStream = new FileStream(this.PaidUtilityDataFile, mode);
                        StreamWriter UtilityWritter = new StreamWriter(UtilityStream);

                        if (viaCard)
                            UtilityWritter.WriteLine("card");
                        else
                            UtilityWritter.WriteLine("wallet");
                        UtilityWritter.WriteLine("Service........ " + this.UtilityFees[(int)services[i] - 1].Service);
                        UtilityWritter.WriteLine("PerUnitFee..... " + this.UtilityFees[(int)services[i] - 1].PerUnitFee);
                        UtilityWritter.WriteLine("UnitsSpent..... " + this.UtilityFees[(int)services[i] - 1].UnitsSpent);
                        UtilityWritter.WriteLine("UpTo........... " + this.UtilityFees[(int)services[i] - 1].UpTo);
                        UtilityWritter.WriteLine("Debt........... " + this.UtilityFees[(int)services[i] - 1].Debt);
                        UtilityWritter.WriteLine("Paid........... " + this.UtilityFees[(int)services[i] - 1].Paid);
                        UtilityWritter.WriteLine("PaymentDate.... " + this.UtilityFees[(int)services[i] - 1].PaymentDate);
                        UtilityWritter.WriteLine(".....................................");
                        UtilityWritter.Close();
                    }
                    else
                    {
                        status = "One or more payments cannot be completed due to insufficient balance.";
                        return false;
                    }
                }
                status = "OK";
                return true;
            }
            catch(Exception e)
            {
                status = e.Message;
                return false;
            }
        }
        public bool Pay(decimal cost, string placeOrShop, string itemName, bool viaCard, out string status)
        {
            Account CardOrWallet = this.ServiceWallet;
            if (viaCard)
                CardOrWallet = this.BankCard;
            try
            {
                if (cost <= CardOrWallet.Balance)
                {
                    EntertainmentOrShopping newItem = new EntertainmentOrShopping(placeOrShop, itemName, cost);
                    this.EntOrShop.Add(newItem);
                    CardOrWallet.Balance -= cost;

                    FileMode mode = FileMode.Create;
                    if (File.Exists(PaidUtilityDataFile))
                        mode = FileMode.Append;

                    FileStream ShopStream = new FileStream(this.ShopAndEntDataFile, mode);
                    StreamWriter ShopsWritter = new StreamWriter(ShopStream);

                    if(viaCard)
                        ShopsWritter.WriteLine("card");
                    else
                        ShopsWritter.WriteLine("wallet");
                    ShopsWritter.WriteLine("Item Name........ " + newItem.ItemName);
                    ShopsWritter.WriteLine("Shop............. " + newItem.PlaceOrShop);
                    ShopsWritter.WriteLine("Cost............. " + newItem.Cost);
                    ShopsWritter.WriteLine("Paid Date........ " + newItem.PaidDate);
                    ShopsWritter.WriteLine(".....................................");
                    ShopsWritter.WriteLine();
                    ShopsWritter.Close();

                    status = "OK";
                    return true;
                }
                status = "Balance is not enough";
                return false;                
            }
            catch(IOException e)
            {
                status = e.Message;
                return false;
            }
        }
        public string PrintCardStatement(out string status)
        {
            try
            {
                string statement = "";
                string temp = "";
                if(File.Exists(ShopAndEntDataFile))
                {
                    FileStream StatementStream = new FileStream(this.ShopAndEntDataFile, FileMode.Open);
                    StreamReader StatementReader = new StreamReader(StatementStream);
                    while((temp = StatementReader.ReadLine()) != null)
                    {
                        if (temp == "card")
                            for (int s = 1; s <= 5; s++)
                                statement += string.Format("{0}\n", StatementReader.ReadLine());
                    }

                    StatementStream.Close();
                    StatementReader.Close();
                }

                if (File.Exists(PaidUtilityDataFile))
                {
                    FileStream StatementStream2 = new FileStream(this.PaidUtilityDataFile, FileMode.Open);
                    StreamReader StatementReader2 = new StreamReader(StatementStream2);
                    temp = "";
                    while ((temp = StatementReader2.ReadLine()) != null)
                    {
                        if (temp == "card")
                            for (int s = 1; s <= 8; s++)
                                statement += string.Format("{0}\n", StatementReader2.ReadLine());
                    }

                    StatementStream2.Close();
                    StatementReader2.Close();
                }
                status = "OK";
                return statement;
            }
            catch(FileNotFoundException e)
            {
                status = e.Message;
                return "";
            }
            catch(IOException e)
            {
                status = e.Message;
                return "";
            }
        }
        public string WalletPaymentHistory(out string status)
        {
            try
            {
                string statement = "";
                string temp = "";
                if(File.Exists(this.ShopAndEntDataFile))
                {
                    FileStream StatementStream = new FileStream(this.ShopAndEntDataFile, FileMode.Open);
                    StreamReader StatementReader = new StreamReader(StatementStream);
                    while ((temp = StatementReader.ReadLine()) != null)
                    {
                        if (temp == "wallet")
                            for (int s = 1; s <= 5; s++)
                                statement += string.Format("{0}\n", StatementReader.ReadLine());
                    }

                    StatementStream.Close();
                    StatementReader.Close();
                }

                if(File.Exists(this.UtilityDataFile))
                {
                    FileStream StatementStream2 = new FileStream(this.PaidUtilityDataFile, FileMode.Open);
                    StreamReader StatementReader2 = new StreamReader(StatementStream2);
                    temp = "";
                    while ((temp = StatementReader2.ReadLine()) != null)
                    {
                        if (temp == "wallet")
                            for (int s = 1; s <= 8; s++)
                                statement += string.Format("{0}\n", StatementReader2.ReadLine());
                    }

                    StatementStream2.Close();
                    StatementReader2.Close();
                }

                status = "OK";
                return statement;
            }
            catch (FileNotFoundException e)
            {
                status = e.Message;
                return "";
            }
            catch (IOException e)
            {
                status = e.Message;
                return "";
            }
        }
        public bool SaveState(out string status)
        {
            try
            {
                FileStream LastDataStream = new FileStream(this.UtilityDataFile, FileMode.Create);
                BinaryWriter BinaryData = new BinaryWriter(LastDataStream);

                for (int i = 0; i < UtilityFees.Length; i++)
                {
                    BinaryData.Write((int)UtilityFees[i].Service);
                    BinaryData.Write(UtilityFees[i].PerUnitFee);
                    BinaryData.Write(UtilityFees[i].UnitsSpent);
                    BinaryData.Write(UtilityFees[i].UpTo);
                    BinaryData.Write(UtilityFees[i].Debt);
                    BinaryData.Write(UtilityFees[i].Paid);
                    BinaryData.Write(UtilityFees[i].PaymentDate);
                }

                LastDataStream.Close();
                BinaryData.Close();

                FileStream AccountStream = new FileStream(this.ServiceWalletDataFile, FileMode.Create);
                BinaryWriter WalletDataWritter = new BinaryWriter(AccountStream);
                WalletDataWritter.Write(this.ServiceWallet.DisplayName);
                WalletDataWritter.Write(this.ServiceWallet.PIN);
                WalletDataWritter.Write(this.ServiceWallet.Balance);

                AccountStream.Close();
                WalletDataWritter.Close();

                AccountStream = new FileStream(this.CardDataFile, FileMode.Create);
                BinaryWriter CardDataWritter = new BinaryWriter(AccountStream);
                CardDataWritter.Write(this.Card.BankName);
                CardDataWritter.Write(this.Card.PIN);
                CardDataWritter.Write(this.Card.Balance);

                AccountStream.Close();
                CardDataWritter.Close();

                status = "OK";
                return true;
            }
            catch (IOException e)
            {
                status = e.Message;
                return false;
            }
        }
    }
}