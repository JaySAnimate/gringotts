﻿namespace Gringotts
{
    partial class LogInUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LogInUI));
            this.PasswordBox = new System.Windows.Forms.TextBox();
            this.LogInBox = new System.Windows.Forms.TextBox();
            this.GringottsWallet = new System.Windows.Forms.Label();
            this.UsernameLabel = new System.Windows.Forms.Label();
            this.PasswordLabel = new System.Windows.Forms.Label();
            this.LogInButton = new System.Windows.Forms.Button();
            this.MainSignUpButton = new System.Windows.Forms.Button();
            this.XButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // PasswordBox
            // 
            this.PasswordBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(83)))), ((int)(((byte)(96)))));
            this.PasswordBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PasswordBox.Font = new System.Drawing.Font("Calibri", 15F);
            this.PasswordBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.PasswordBox.Location = new System.Drawing.Point(55, 256);
            this.PasswordBox.Name = "PasswordBox";
            this.PasswordBox.Size = new System.Drawing.Size(300, 32);
            this.PasswordBox.TabIndex = 1;
            this.PasswordBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // LogInBox
            // 
            this.LogInBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(83)))), ((int)(((byte)(96)))));
            this.LogInBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LogInBox.Font = new System.Drawing.Font("Calibri", 15F);
            this.LogInBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.LogInBox.Location = new System.Drawing.Point(55, 185);
            this.LogInBox.Name = "LogInBox";
            this.LogInBox.Size = new System.Drawing.Size(300, 32);
            this.LogInBox.TabIndex = 0;
            this.LogInBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // GringottsWallet
            // 
            this.GringottsWallet.AutoSize = true;
            this.GringottsWallet.Font = new System.Drawing.Font("Calibri", 20F, System.Drawing.FontStyle.Bold);
            this.GringottsWallet.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.GringottsWallet.Location = new System.Drawing.Point(106, 83);
            this.GringottsWallet.Name = "GringottsWallet";
            this.GringottsWallet.Size = new System.Drawing.Size(199, 33);
            this.GringottsWallet.TabIndex = 12;
            this.GringottsWallet.Text = "Gringotts Wallet";
            // 
            // UsernameLabel
            // 
            this.UsernameLabel.AutoSize = true;
            this.UsernameLabel.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.UsernameLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.UsernameLabel.Location = new System.Drawing.Point(52, 165);
            this.UsernameLabel.Name = "UsernameLabel";
            this.UsernameLabel.Size = new System.Drawing.Size(68, 17);
            this.UsernameLabel.TabIndex = 13;
            this.UsernameLabel.Text = "Username";
            // 
            // PasswordLabel
            // 
            this.PasswordLabel.AutoSize = true;
            this.PasswordLabel.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.PasswordLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.PasswordLabel.Location = new System.Drawing.Point(52, 236);
            this.PasswordLabel.Name = "PasswordLabel";
            this.PasswordLabel.Size = new System.Drawing.Size(65, 17);
            this.PasswordLabel.TabIndex = 14;
            this.PasswordLabel.Text = "Password";
            // 
            // LogInButton
            // 
            this.LogInButton.FlatAppearance.BorderSize = 0;
            this.LogInButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LogInButton.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LogInButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.LogInButton.Location = new System.Drawing.Point(55, 306);
            this.LogInButton.Name = "LogInButton";
            this.LogInButton.Size = new System.Drawing.Size(300, 40);
            this.LogInButton.TabIndex = 15;
            this.LogInButton.Text = "Log In";
            this.LogInButton.UseVisualStyleBackColor = true;
            this.LogInButton.Click += new System.EventHandler(this.LogInButton_Click);
            // 
            // MainSignUpButton
            // 
            this.MainSignUpButton.FlatAppearance.BorderSize = 0;
            this.MainSignUpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MainSignUpButton.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainSignUpButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.MainSignUpButton.Location = new System.Drawing.Point(55, 364);
            this.MainSignUpButton.Name = "MainSignUpButton";
            this.MainSignUpButton.Size = new System.Drawing.Size(300, 40);
            this.MainSignUpButton.TabIndex = 16;
            this.MainSignUpButton.Text = "Sign Up";
            this.MainSignUpButton.UseVisualStyleBackColor = true;
            this.MainSignUpButton.Click += new System.EventHandler(this.MainSignUpButton_Click);
            // 
            // XButton
            // 
            this.XButton.FlatAppearance.BorderSize = 0;
            this.XButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.XButton.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.XButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.XButton.Location = new System.Drawing.Point(353, 12);
            this.XButton.Name = "XButton";
            this.XButton.Size = new System.Drawing.Size(35, 35);
            this.XButton.TabIndex = 20;
            this.XButton.Text = " X";
            this.XButton.UseVisualStyleBackColor = true;
            this.XButton.Click += new System.EventHandler(this.XButton_Click);
            // 
            // LogInUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(83)))), ((int)(((byte)(96)))));
            this.ClientSize = new System.Drawing.Size(400, 555);
            this.Controls.Add(this.XButton);
            this.Controls.Add(this.MainSignUpButton);
            this.Controls.Add(this.LogInButton);
            this.Controls.Add(this.PasswordLabel);
            this.Controls.Add(this.UsernameLabel);
            this.Controls.Add(this.GringottsWallet);
            this.Controls.Add(this.LogInBox);
            this.Controls.Add(this.PasswordBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LogInUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LogInUI";
            this.Load += new System.EventHandler(this.LogInUI_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox PasswordBox;
        private System.Windows.Forms.TextBox LogInBox;
        private System.Windows.Forms.Label GringottsWallet;
        private System.Windows.Forms.Label UsernameLabel;
        private System.Windows.Forms.Label PasswordLabel;
        private System.Windows.Forms.Button LogInButton;
        private System.Windows.Forms.Button MainSignUpButton;
        private System.Windows.Forms.Button XButton;
    }
}