﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gringotts
{
    public partial class ConfigureWallet : Form
    {   
        AddBankCard CardConfig;

        public ConfigureWallet()
        {
            InitializeComponent();
        }

        private void ConfigWalletDoneButton_Click(object sender, EventArgs e)
        {
            bool success = true;
            try
            {
                if (DisplayNameBox.Text != "")
                {
                    foreach (var letter in DisplayNameBox.Text)
                        if (!(letter >= 'A' && letter <= 'Z' || letter >= 'a' && letter <= 'z') || !(Int32.Parse(PINBox.Text).ToString().Length == 6))
                        {
                            success = false;
                            MessageBox.Show("Invalid Credentials");
                        }
                }
                else success = false;


                if (success)
                {
                    CardConfig = new AddBankCard(DisplayNameBox.Text, Int32.Parse(PINBox.Text));
                    CardConfig.Show();
                    this.Hide();
                    MessageBox.Show("Success!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}