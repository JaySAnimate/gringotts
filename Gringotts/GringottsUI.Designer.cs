﻿namespace Gringotts
{
    partial class Gringotts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Gringotts));
            this.MainPanel = new System.Windows.Forms.Panel();
            this.WalletUserName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.UtilityPayPanel = new System.Windows.Forms.Panel();
            this.viacard = new System.Windows.Forms.CheckBox();
            this.UPanelClose = new System.Windows.Forms.Button();
            this.UPayButton = new System.Windows.Forms.Button();
            this.UFee = new System.Windows.Forms.Label();
            this.Uname = new System.Windows.Forms.Label();
            this.TopUpPanel = new System.Windows.Forms.Panel();
            this.ToWalletBox = new System.Windows.Forms.TextBox();
            this.TopUpClose = new System.Windows.Forms.Button();
            this.ToWallet = new System.Windows.Forms.Button();
            this.CashInPanel = new System.Windows.Forms.Panel();
            this.CashInBox = new System.Windows.Forms.TextBox();
            this.CashInClose = new System.Windows.Forms.Button();
            this.SubmitCash = new System.Windows.Forms.Button();
            this.XButton = new System.Windows.Forms.Button();
            this.MarketsButton = new System.Windows.Forms.Button();
            this.ShopsButton = new System.Windows.Forms.Button();
            this.ConcertsButton = new System.Windows.Forms.Button();
            this.PubsButton = new System.Windows.Forms.Button();
            this.MoviesButton = new System.Windows.Forms.Button();
            this.WalletBalance = new System.Windows.Forms.Label();
            this.CardBalance = new System.Windows.Forms.Label();
            this.CardLabel = new System.Windows.Forms.Label();
            this.WalletLabel = new System.Windows.Forms.Label();
            this.HistoryButton = new System.Windows.Forms.Button();
            this.StatementButton = new System.Windows.Forms.Button();
            this.TopUpButton = new System.Windows.Forms.Button();
            this.MainCashIn = new System.Windows.Forms.Button();
            this.TVButton = new System.Windows.Forms.Button();
            this.InternetButton = new System.Windows.Forms.Button();
            this.PhoneButton = new System.Windows.Forms.Button();
            this.GasButton = new System.Windows.Forms.Button();
            this.WaterButton = new System.Windows.Forms.Button();
            this.ElectricityButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.MainPanel.SuspendLayout();
            this.UtilityPayPanel.SuspendLayout();
            this.TopUpPanel.SuspendLayout();
            this.CashInPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainPanel
            // 
            this.MainPanel.Controls.Add(this.button1);
            this.MainPanel.Controls.Add(this.WalletUserName);
            this.MainPanel.Controls.Add(this.label1);
            this.MainPanel.Controls.Add(this.UtilityPayPanel);
            this.MainPanel.Controls.Add(this.TopUpPanel);
            this.MainPanel.Controls.Add(this.CashInPanel);
            this.MainPanel.Controls.Add(this.XButton);
            this.MainPanel.Controls.Add(this.MarketsButton);
            this.MainPanel.Controls.Add(this.ShopsButton);
            this.MainPanel.Controls.Add(this.ConcertsButton);
            this.MainPanel.Controls.Add(this.PubsButton);
            this.MainPanel.Controls.Add(this.MoviesButton);
            this.MainPanel.Controls.Add(this.WalletBalance);
            this.MainPanel.Controls.Add(this.CardBalance);
            this.MainPanel.Controls.Add(this.CardLabel);
            this.MainPanel.Controls.Add(this.WalletLabel);
            this.MainPanel.Controls.Add(this.HistoryButton);
            this.MainPanel.Controls.Add(this.StatementButton);
            this.MainPanel.Controls.Add(this.TopUpButton);
            this.MainPanel.Controls.Add(this.MainCashIn);
            this.MainPanel.Controls.Add(this.TVButton);
            this.MainPanel.Controls.Add(this.InternetButton);
            this.MainPanel.Controls.Add(this.PhoneButton);
            this.MainPanel.Controls.Add(this.GasButton);
            this.MainPanel.Controls.Add(this.WaterButton);
            this.MainPanel.Controls.Add(this.ElectricityButton);
            this.MainPanel.Location = new System.Drawing.Point(0, 0);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(1280, 720);
            this.MainPanel.TabIndex = 0;
            // 
            // WalletUserName
            // 
            this.WalletUserName.AutoSize = true;
            this.WalletUserName.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold);
            this.WalletUserName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.WalletUserName.Location = new System.Drawing.Point(12, 686);
            this.WalletUserName.Name = "WalletUserName";
            this.WalletUserName.Size = new System.Drawing.Size(95, 24);
            this.WalletUserName.TabIndex = 26;
            this.WalletUserName.Text = "Username";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.label1.Location = new System.Drawing.Point(57, 135);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 24);
            this.label1.TabIndex = 25;
            this.label1.Text = "// To Do";
            // 
            // UtilityPayPanel
            // 
            this.UtilityPayPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UtilityPayPanel.Controls.Add(this.viacard);
            this.UtilityPayPanel.Controls.Add(this.UPanelClose);
            this.UtilityPayPanel.Controls.Add(this.UPayButton);
            this.UtilityPayPanel.Controls.Add(this.UFee);
            this.UtilityPayPanel.Controls.Add(this.Uname);
            this.UtilityPayPanel.Location = new System.Drawing.Point(410, 155);
            this.UtilityPayPanel.Name = "UtilityPayPanel";
            this.UtilityPayPanel.Size = new System.Drawing.Size(293, 398);
            this.UtilityPayPanel.TabIndex = 20;
            // 
            // viacard
            // 
            this.viacard.AutoSize = true;
            this.viacard.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.viacard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.viacard.Location = new System.Drawing.Point(106, 254);
            this.viacard.Name = "viacard";
            this.viacard.Size = new System.Drawing.Size(89, 21);
            this.viacard.TabIndex = 23;
            this.viacard.Text = "Via Card";
            this.viacard.UseVisualStyleBackColor = true;
            // 
            // UPanelClose
            // 
            this.UPanelClose.FlatAppearance.BorderSize = 0;
            this.UPanelClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UPanelClose.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.UPanelClose.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.UPanelClose.Location = new System.Drawing.Point(253, 3);
            this.UPanelClose.Name = "UPanelClose";
            this.UPanelClose.Size = new System.Drawing.Size(35, 35);
            this.UPanelClose.TabIndex = 22;
            this.UPanelClose.Text = " X";
            this.UPanelClose.UseVisualStyleBackColor = true;
            this.UPanelClose.Click += new System.EventHandler(this.UPanelClose_Click);
            // 
            // UPayButton
            // 
            this.UPayButton.FlatAppearance.BorderSize = 0;
            this.UPayButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UPayButton.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UPayButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.UPayButton.Location = new System.Drawing.Point(30, 295);
            this.UPayButton.Name = "UPayButton";
            this.UPayButton.Size = new System.Drawing.Size(239, 40);
            this.UPayButton.TabIndex = 21;
            this.UPayButton.Text = "Pay";
            this.UPayButton.UseVisualStyleBackColor = true;
            this.UPayButton.Click += new System.EventHandler(this.UPayButton_Click);
            // 
            // UFee
            // 
            this.UFee.AutoSize = true;
            this.UFee.Font = new System.Drawing.Font("Calibri", 20F, System.Drawing.FontStyle.Bold);
            this.UFee.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.UFee.Location = new System.Drawing.Point(82, 144);
            this.UFee.Name = "UFee";
            this.UFee.Size = new System.Drawing.Size(131, 33);
            this.UFee.TabIndex = 12;
            this.UFee.Text = "Utility Fee";
            // 
            // Uname
            // 
            this.Uname.AutoSize = true;
            this.Uname.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold);
            this.Uname.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.Uname.Location = new System.Drawing.Point(84, 57);
            this.Uname.Name = "Uname";
            this.Uname.Size = new System.Drawing.Size(115, 24);
            this.Uname.TabIndex = 11;
            this.Uname.Text = "Utility Name";
            // 
            // TopUpPanel
            // 
            this.TopUpPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TopUpPanel.Controls.Add(this.ToWalletBox);
            this.TopUpPanel.Controls.Add(this.TopUpClose);
            this.TopUpPanel.Controls.Add(this.ToWallet);
            this.TopUpPanel.Location = new System.Drawing.Point(776, 380);
            this.TopUpPanel.Name = "TopUpPanel";
            this.TopUpPanel.Size = new System.Drawing.Size(293, 191);
            this.TopUpPanel.TabIndex = 24;
            // 
            // ToWalletBox
            // 
            this.ToWalletBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(83)))), ((int)(((byte)(96)))));
            this.ToWalletBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ToWalletBox.Font = new System.Drawing.Font("Calibri", 15F);
            this.ToWalletBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.ToWalletBox.Location = new System.Drawing.Point(73, 52);
            this.ToWalletBox.Name = "ToWalletBox";
            this.ToWalletBox.Size = new System.Drawing.Size(154, 32);
            this.ToWalletBox.TabIndex = 23;
            this.ToWalletBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TopUpClose
            // 
            this.TopUpClose.FlatAppearance.BorderSize = 0;
            this.TopUpClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TopUpClose.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.TopUpClose.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.TopUpClose.Location = new System.Drawing.Point(253, 3);
            this.TopUpClose.Name = "TopUpClose";
            this.TopUpClose.Size = new System.Drawing.Size(35, 35);
            this.TopUpClose.TabIndex = 22;
            this.TopUpClose.Text = " X";
            this.TopUpClose.UseVisualStyleBackColor = true;
            this.TopUpClose.Click += new System.EventHandler(this.TopUpClose_Click);
            // 
            // ToWallet
            // 
            this.ToWallet.FlatAppearance.BorderSize = 0;
            this.ToWallet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ToWallet.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToWallet.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.ToWallet.Location = new System.Drawing.Point(28, 117);
            this.ToWallet.Name = "ToWallet";
            this.ToWallet.Size = new System.Drawing.Size(239, 40);
            this.ToWallet.TabIndex = 21;
            this.ToWallet.Text = "Add To Wallet";
            this.ToWallet.UseVisualStyleBackColor = true;
            this.ToWallet.Click += new System.EventHandler(this.ToWallet_Click);
            // 
            // CashInPanel
            // 
            this.CashInPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CashInPanel.Controls.Add(this.CashInBox);
            this.CashInPanel.Controls.Add(this.CashInClose);
            this.CashInPanel.Controls.Add(this.SubmitCash);
            this.CashInPanel.Location = new System.Drawing.Point(396, 380);
            this.CashInPanel.Name = "CashInPanel";
            this.CashInPanel.Size = new System.Drawing.Size(293, 191);
            this.CashInPanel.TabIndex = 23;
            // 
            // CashInBox
            // 
            this.CashInBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(83)))), ((int)(((byte)(96)))));
            this.CashInBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CashInBox.Font = new System.Drawing.Font("Calibri", 15F);
            this.CashInBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.CashInBox.Location = new System.Drawing.Point(73, 52);
            this.CashInBox.Name = "CashInBox";
            this.CashInBox.Size = new System.Drawing.Size(154, 32);
            this.CashInBox.TabIndex = 23;
            this.CashInBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // CashInClose
            // 
            this.CashInClose.FlatAppearance.BorderSize = 0;
            this.CashInClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CashInClose.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.CashInClose.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.CashInClose.Location = new System.Drawing.Point(253, 3);
            this.CashInClose.Name = "CashInClose";
            this.CashInClose.Size = new System.Drawing.Size(35, 35);
            this.CashInClose.TabIndex = 22;
            this.CashInClose.Text = " X";
            this.CashInClose.UseVisualStyleBackColor = true;
            this.CashInClose.Click += new System.EventHandler(this.CashInClose_Click);
            // 
            // SubmitCash
            // 
            this.SubmitCash.FlatAppearance.BorderSize = 0;
            this.SubmitCash.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SubmitCash.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubmitCash.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.SubmitCash.Location = new System.Drawing.Point(28, 117);
            this.SubmitCash.Name = "SubmitCash";
            this.SubmitCash.Size = new System.Drawing.Size(239, 40);
            this.SubmitCash.TabIndex = 21;
            this.SubmitCash.Text = "Cash In";
            this.SubmitCash.UseVisualStyleBackColor = true;
            this.SubmitCash.Click += new System.EventHandler(this.SubmitCash_Click);
            // 
            // XButton
            // 
            this.XButton.FlatAppearance.BorderSize = 0;
            this.XButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.XButton.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.XButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.XButton.Location = new System.Drawing.Point(1238, 8);
            this.XButton.Name = "XButton";
            this.XButton.Size = new System.Drawing.Size(35, 35);
            this.XButton.TabIndex = 19;
            this.XButton.Text = " X";
            this.XButton.UseVisualStyleBackColor = true;
            this.XButton.Click += new System.EventHandler(this.XButton_Click);
            // 
            // MarketsButton
            // 
            this.MarketsButton.FlatAppearance.BorderSize = 0;
            this.MarketsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MarketsButton.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MarketsButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.MarketsButton.Image = ((System.Drawing.Image)(resources.GetObject("MarketsButton.Image")));
            this.MarketsButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.MarketsButton.Location = new System.Drawing.Point(16, 430);
            this.MarketsButton.Name = "MarketsButton";
            this.MarketsButton.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.MarketsButton.Size = new System.Drawing.Size(234, 40);
            this.MarketsButton.TabIndex = 18;
            this.MarketsButton.Text = "Markets";
            this.MarketsButton.UseVisualStyleBackColor = true;
            // 
            // ShopsButton
            // 
            this.ShopsButton.FlatAppearance.BorderSize = 0;
            this.ShopsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ShopsButton.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ShopsButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.ShopsButton.Image = ((System.Drawing.Image)(resources.GetObject("ShopsButton.Image")));
            this.ShopsButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ShopsButton.Location = new System.Drawing.Point(16, 365);
            this.ShopsButton.Name = "ShopsButton";
            this.ShopsButton.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.ShopsButton.Size = new System.Drawing.Size(234, 40);
            this.ShopsButton.TabIndex = 17;
            this.ShopsButton.Text = "Shops";
            this.ShopsButton.UseVisualStyleBackColor = true;
            // 
            // ConcertsButton
            // 
            this.ConcertsButton.FlatAppearance.BorderSize = 0;
            this.ConcertsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ConcertsButton.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConcertsButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.ConcertsButton.Image = ((System.Drawing.Image)(resources.GetObject("ConcertsButton.Image")));
            this.ConcertsButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ConcertsButton.Location = new System.Drawing.Point(16, 300);
            this.ConcertsButton.Name = "ConcertsButton";
            this.ConcertsButton.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.ConcertsButton.Size = new System.Drawing.Size(234, 40);
            this.ConcertsButton.TabIndex = 16;
            this.ConcertsButton.Text = "Concerts";
            this.ConcertsButton.UseVisualStyleBackColor = true;
            // 
            // PubsButton
            // 
            this.PubsButton.FlatAppearance.BorderSize = 0;
            this.PubsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PubsButton.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PubsButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.PubsButton.Image = ((System.Drawing.Image)(resources.GetObject("PubsButton.Image")));
            this.PubsButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.PubsButton.Location = new System.Drawing.Point(16, 236);
            this.PubsButton.Name = "PubsButton";
            this.PubsButton.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.PubsButton.Size = new System.Drawing.Size(234, 40);
            this.PubsButton.TabIndex = 15;
            this.PubsButton.Text = "Pubs";
            this.PubsButton.UseVisualStyleBackColor = true;
            // 
            // MoviesButton
            // 
            this.MoviesButton.FlatAppearance.BorderSize = 0;
            this.MoviesButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MoviesButton.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MoviesButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.MoviesButton.Image = ((System.Drawing.Image)(resources.GetObject("MoviesButton.Image")));
            this.MoviesButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.MoviesButton.Location = new System.Drawing.Point(16, 173);
            this.MoviesButton.Name = "MoviesButton";
            this.MoviesButton.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.MoviesButton.Size = new System.Drawing.Size(234, 40);
            this.MoviesButton.TabIndex = 14;
            this.MoviesButton.Text = " Movies";
            this.MoviesButton.UseVisualStyleBackColor = true;
            // 
            // WalletBalance
            // 
            this.WalletBalance.AutoSize = true;
            this.WalletBalance.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold);
            this.WalletBalance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.WalletBalance.Location = new System.Drawing.Point(158, 9);
            this.WalletBalance.Name = "WalletBalance";
            this.WalletBalance.Size = new System.Drawing.Size(20, 24);
            this.WalletBalance.TabIndex = 13;
            this.WalletBalance.Text = "0";
            // 
            // CardBalance
            // 
            this.CardBalance.AutoSize = true;
            this.CardBalance.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold);
            this.CardBalance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.CardBalance.Location = new System.Drawing.Point(158, 38);
            this.CardBalance.Name = "CardBalance";
            this.CardBalance.Size = new System.Drawing.Size(20, 24);
            this.CardBalance.TabIndex = 12;
            this.CardBalance.Text = "0";
            // 
            // CardLabel
            // 
            this.CardLabel.AutoSize = true;
            this.CardLabel.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold);
            this.CardLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.CardLabel.Location = new System.Drawing.Point(27, 38);
            this.CardLabel.Name = "CardLabel";
            this.CardLabel.Size = new System.Drawing.Size(125, 24);
            this.CardLabel.TabIndex = 11;
            this.CardLabel.Text = "Card Balance:";
            // 
            // WalletLabel
            // 
            this.WalletLabel.AutoSize = true;
            this.WalletLabel.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold);
            this.WalletLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.WalletLabel.Location = new System.Drawing.Point(12, 9);
            this.WalletLabel.Name = "WalletLabel";
            this.WalletLabel.Size = new System.Drawing.Size(140, 24);
            this.WalletLabel.TabIndex = 10;
            this.WalletLabel.Text = "Wallet Balance:";
            // 
            // HistoryButton
            // 
            this.HistoryButton.FlatAppearance.BorderSize = 0;
            this.HistoryButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HistoryButton.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HistoryButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.HistoryButton.Location = new System.Drawing.Point(754, 642);
            this.HistoryButton.Name = "HistoryButton";
            this.HistoryButton.Size = new System.Drawing.Size(349, 40);
            this.HistoryButton.TabIndex = 9;
            this.HistoryButton.Text = "Payment History";
            this.HistoryButton.UseVisualStyleBackColor = true;
            this.HistoryButton.Click += new System.EventHandler(this.HistoryButton_Click);
            // 
            // StatementButton
            // 
            this.StatementButton.FlatAppearance.BorderSize = 0;
            this.StatementButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StatementButton.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatementButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.StatementButton.Location = new System.Drawing.Point(376, 642);
            this.StatementButton.Name = "StatementButton";
            this.StatementButton.Size = new System.Drawing.Size(349, 40);
            this.StatementButton.TabIndex = 8;
            this.StatementButton.Text = "Print Card Statement";
            this.StatementButton.UseVisualStyleBackColor = true;
            this.StatementButton.Click += new System.EventHandler(this.StatementButton_Click);
            // 
            // TopUpButton
            // 
            this.TopUpButton.FlatAppearance.BorderSize = 0;
            this.TopUpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TopUpButton.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TopUpButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.TopUpButton.Location = new System.Drawing.Point(754, 577);
            this.TopUpButton.Name = "TopUpButton";
            this.TopUpButton.Size = new System.Drawing.Size(349, 40);
            this.TopUpButton.TabIndex = 7;
            this.TopUpButton.Text = "Top Up From Card";
            this.TopUpButton.UseVisualStyleBackColor = true;
            this.TopUpButton.Click += new System.EventHandler(this.TopUpButton_Click);
            // 
            // MainCashIn
            // 
            this.MainCashIn.FlatAppearance.BorderSize = 0;
            this.MainCashIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MainCashIn.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainCashIn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.MainCashIn.Location = new System.Drawing.Point(376, 577);
            this.MainCashIn.Name = "MainCashIn";
            this.MainCashIn.Size = new System.Drawing.Size(349, 40);
            this.MainCashIn.TabIndex = 6;
            this.MainCashIn.Text = "Cash In";
            this.MainCashIn.UseVisualStyleBackColor = true;
            this.MainCashIn.Click += new System.EventHandler(this.MainCashIn_Click);
            // 
            // TVButton
            // 
            this.TVButton.FlatAppearance.BorderSize = 0;
            this.TVButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TVButton.Image = ((System.Drawing.Image)(resources.GetObject("TVButton.Image")));
            this.TVButton.Location = new System.Drawing.Point(903, 328);
            this.TVButton.Name = "TVButton";
            this.TVButton.Size = new System.Drawing.Size(200, 200);
            this.TVButton.TabIndex = 5;
            this.TVButton.UseVisualStyleBackColor = true;
            this.TVButton.Click += new System.EventHandler(this.TVButton_Click);
            // 
            // InternetButton
            // 
            this.InternetButton.FlatAppearance.BorderSize = 0;
            this.InternetButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InternetButton.Image = ((System.Drawing.Image)(resources.GetObject("InternetButton.Image")));
            this.InternetButton.Location = new System.Drawing.Point(638, 328);
            this.InternetButton.Name = "InternetButton";
            this.InternetButton.Size = new System.Drawing.Size(200, 200);
            this.InternetButton.TabIndex = 4;
            this.InternetButton.UseVisualStyleBackColor = true;
            this.InternetButton.Click += new System.EventHandler(this.InternetButton_Click);
            // 
            // PhoneButton
            // 
            this.PhoneButton.FlatAppearance.BorderSize = 0;
            this.PhoneButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PhoneButton.Image = ((System.Drawing.Image)(resources.GetObject("PhoneButton.Image")));
            this.PhoneButton.Location = new System.Drawing.Point(376, 328);
            this.PhoneButton.Name = "PhoneButton";
            this.PhoneButton.Size = new System.Drawing.Size(200, 200);
            this.PhoneButton.TabIndex = 3;
            this.PhoneButton.UseVisualStyleBackColor = true;
            this.PhoneButton.Click += new System.EventHandler(this.PhoneButton_Click);
            // 
            // GasButton
            // 
            this.GasButton.FlatAppearance.BorderSize = 0;
            this.GasButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GasButton.Image = ((System.Drawing.Image)(resources.GetObject("GasButton.Image")));
            this.GasButton.Location = new System.Drawing.Point(903, 38);
            this.GasButton.Name = "GasButton";
            this.GasButton.Size = new System.Drawing.Size(200, 200);
            this.GasButton.TabIndex = 2;
            this.GasButton.UseVisualStyleBackColor = true;
            this.GasButton.Click += new System.EventHandler(this.GasButton_Click);
            // 
            // WaterButton
            // 
            this.WaterButton.FlatAppearance.BorderSize = 0;
            this.WaterButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.WaterButton.Image = ((System.Drawing.Image)(resources.GetObject("WaterButton.Image")));
            this.WaterButton.Location = new System.Drawing.Point(638, 38);
            this.WaterButton.Name = "WaterButton";
            this.WaterButton.Size = new System.Drawing.Size(200, 200);
            this.WaterButton.TabIndex = 1;
            this.WaterButton.UseVisualStyleBackColor = true;
            this.WaterButton.Click += new System.EventHandler(this.WaterButton_Click);
            // 
            // ElectricityButton
            // 
            this.ElectricityButton.FlatAppearance.BorderSize = 0;
            this.ElectricityButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ElectricityButton.Image = ((System.Drawing.Image)(resources.GetObject("ElectricityButton.Image")));
            this.ElectricityButton.Location = new System.Drawing.Point(376, 38);
            this.ElectricityButton.Name = "ElectricityButton";
            this.ElectricityButton.Size = new System.Drawing.Size(200, 200);
            this.ElectricityButton.TabIndex = 0;
            this.ElectricityButton.UseVisualStyleBackColor = true;
            this.ElectricityButton.Click += new System.EventHandler(this.ElectricityButton_Click);
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.button1.Location = new System.Drawing.Point(1131, 577);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(125, 121);
            this.button1.TabIndex = 27;
            this.button1.Text = "Receive Fees";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Gringotts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(83)))), ((int)(((byte)(96)))));
            this.ClientSize = new System.Drawing.Size(1280, 720);
            this.Controls.Add(this.MainPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Gringotts";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.MainPanel.ResumeLayout(false);
            this.MainPanel.PerformLayout();
            this.UtilityPayPanel.ResumeLayout(false);
            this.UtilityPayPanel.PerformLayout();
            this.TopUpPanel.ResumeLayout(false);
            this.TopUpPanel.PerformLayout();
            this.CashInPanel.ResumeLayout(false);
            this.CashInPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel MainPanel;
        private System.Windows.Forms.Button ElectricityButton;
        private System.Windows.Forms.Button GasButton;
        private System.Windows.Forms.Button WaterButton;
        private System.Windows.Forms.Button TVButton;
        private System.Windows.Forms.Button InternetButton;
        private System.Windows.Forms.Button PhoneButton;
        private System.Windows.Forms.Button MainCashIn;
        private System.Windows.Forms.Button ConcertsButton;
        private System.Windows.Forms.Button PubsButton;
        private System.Windows.Forms.Button MoviesButton;
        private System.Windows.Forms.Label WalletBalance;
        private System.Windows.Forms.Label CardBalance;
        private System.Windows.Forms.Label CardLabel;
        private System.Windows.Forms.Label WalletLabel;
        private System.Windows.Forms.Button HistoryButton;
        private System.Windows.Forms.Button StatementButton;
        private System.Windows.Forms.Button TopUpButton;
        private System.Windows.Forms.Button MarketsButton;
        private System.Windows.Forms.Button ShopsButton;
        private System.Windows.Forms.Button XButton;
        private System.Windows.Forms.Panel UtilityPayPanel;
        private System.Windows.Forms.Button UPayButton;
        private System.Windows.Forms.Label UFee;
        private System.Windows.Forms.Label Uname;
        private System.Windows.Forms.Button UPanelClose;
        private System.Windows.Forms.Panel CashInPanel;
        private System.Windows.Forms.Button CashInClose;
        private System.Windows.Forms.Button SubmitCash;
        private System.Windows.Forms.TextBox CashInBox;
        private System.Windows.Forms.Panel TopUpPanel;
        private System.Windows.Forms.TextBox ToWalletBox;
        private System.Windows.Forms.Button TopUpClose;
        private System.Windows.Forms.Button ToWallet;
        private System.Windows.Forms.CheckBox viacard;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label WalletUserName;
        private System.Windows.Forms.Button button1;
    }
}

