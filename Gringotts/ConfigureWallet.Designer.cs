﻿namespace Gringotts
{
    partial class ConfigureWallet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigureWallet));
            this.ConfigWalletDoneButton = new System.Windows.Forms.Button();
            this.PINLabel = new System.Windows.Forms.Label();
            this.DisplayNameLabel = new System.Windows.Forms.Label();
            this.GringottsWallet = new System.Windows.Forms.Label();
            this.DisplayNameBox = new System.Windows.Forms.TextBox();
            this.PINBox = new System.Windows.Forms.TextBox();
            this.ConfigureWalletLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ConfigWalletDoneButton
            // 
            this.ConfigWalletDoneButton.FlatAppearance.BorderSize = 0;
            this.ConfigWalletDoneButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ConfigWalletDoneButton.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConfigWalletDoneButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.ConfigWalletDoneButton.Location = new System.Drawing.Point(44, 356);
            this.ConfigWalletDoneButton.Name = "ConfigWalletDoneButton";
            this.ConfigWalletDoneButton.Size = new System.Drawing.Size(300, 40);
            this.ConfigWalletDoneButton.TabIndex = 23;
            this.ConfigWalletDoneButton.Text = "Done";
            this.ConfigWalletDoneButton.UseVisualStyleBackColor = true;
            this.ConfigWalletDoneButton.Click += new System.EventHandler(this.ConfigWalletDoneButton_Click);
            // 
            // PINLabel
            // 
            this.PINLabel.AutoSize = true;
            this.PINLabel.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.PINLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.PINLabel.Location = new System.Drawing.Point(41, 251);
            this.PINLabel.Name = "PINLabel";
            this.PINLabel.Size = new System.Drawing.Size(75, 17);
            this.PINLabel.TabIndex = 21;
            this.PINLabel.Text = "PIN (6 digit)";
            // 
            // DisplayNameLabel
            // 
            this.DisplayNameLabel.AutoSize = true;
            this.DisplayNameLabel.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.DisplayNameLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.DisplayNameLabel.Location = new System.Drawing.Point(41, 180);
            this.DisplayNameLabel.Name = "DisplayNameLabel";
            this.DisplayNameLabel.Size = new System.Drawing.Size(168, 17);
            this.DisplayNameLabel.TabIndex = 20;
            this.DisplayNameLabel.Text = "Display Name (only Letters)";
            // 
            // GringottsWallet
            // 
            this.GringottsWallet.AutoSize = true;
            this.GringottsWallet.Font = new System.Drawing.Font("Calibri", 20F, System.Drawing.FontStyle.Bold);
            this.GringottsWallet.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.GringottsWallet.Location = new System.Drawing.Point(95, 98);
            this.GringottsWallet.Name = "GringottsWallet";
            this.GringottsWallet.Size = new System.Drawing.Size(199, 33);
            this.GringottsWallet.TabIndex = 19;
            this.GringottsWallet.Text = "Gringotts Wallet";
            // 
            // DisplayNameBox
            // 
            this.DisplayNameBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(83)))), ((int)(((byte)(96)))));
            this.DisplayNameBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DisplayNameBox.Font = new System.Drawing.Font("Calibri", 15F);
            this.DisplayNameBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.DisplayNameBox.Location = new System.Drawing.Point(44, 200);
            this.DisplayNameBox.Name = "DisplayNameBox";
            this.DisplayNameBox.Size = new System.Drawing.Size(300, 32);
            this.DisplayNameBox.TabIndex = 1;
            this.DisplayNameBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // PINBox
            // 
            this.PINBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(83)))), ((int)(((byte)(96)))));
            this.PINBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PINBox.Font = new System.Drawing.Font("Calibri", 15F);
            this.PINBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.PINBox.Location = new System.Drawing.Point(44, 271);
            this.PINBox.Name = "PINBox";
            this.PINBox.Size = new System.Drawing.Size(300, 32);
            this.PINBox.TabIndex = 2;
            this.PINBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ConfigureWalletLabel
            // 
            this.ConfigureWalletLabel.AutoSize = true;
            this.ConfigureWalletLabel.Font = new System.Drawing.Font("Calibri", 8F, System.Drawing.FontStyle.Bold);
            this.ConfigureWalletLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.ConfigureWalletLabel.Location = new System.Drawing.Point(154, 134);
            this.ConfigureWalletLabel.Name = "ConfigureWalletLabel";
            this.ConfigureWalletLabel.Size = new System.Drawing.Size(80, 13);
            this.ConfigureWalletLabel.TabIndex = 24;
            this.ConfigureWalletLabel.Text = "Confiure Wallet";
            // 
            // ConfigureWallet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(83)))), ((int)(((byte)(96)))));
            this.ClientSize = new System.Drawing.Size(400, 555);
            this.Controls.Add(this.ConfigureWalletLabel);
            this.Controls.Add(this.ConfigWalletDoneButton);
            this.Controls.Add(this.PINLabel);
            this.Controls.Add(this.DisplayNameLabel);
            this.Controls.Add(this.GringottsWallet);
            this.Controls.Add(this.DisplayNameBox);
            this.Controls.Add(this.PINBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ConfigureWallet";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ConfigureWallet";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ConfigWalletDoneButton;
        private System.Windows.Forms.Label PINLabel;
        private System.Windows.Forms.Label DisplayNameLabel;
        private System.Windows.Forms.Label GringottsWallet;
        private System.Windows.Forms.TextBox DisplayNameBox;
        private System.Windows.Forms.TextBox PINBox;
        private System.Windows.Forms.Label ConfigureWalletLabel;
    }
}