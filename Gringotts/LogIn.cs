﻿using System;
using System.IO;
using System.Linq;

namespace Gringotts
{
    public class LogIn
    {
        public static string _activeUser = "";
        private const string dataFile = @"../../data/UserNamePasswordData.txt";
        
        public bool TrySignUp(string userName, string password)
        {
            if (userName != "" & password != "")
            {
                try
                {
                    if (!File.Exists(dataFile) || !File.ReadLines(dataFile).Any(line => line.Contains(String.Format("{0}:", userName))))
                    {
                        FileStream WriteSigns = new FileStream(dataFile, FileMode.Append);
                        StreamWriter userPasswordWritter = new StreamWriter(WriteSigns);
                        string data = string.Format("{0}:{1}", userName.ToLower(), password);
                        userPasswordWritter.WriteLine(data);
                        userPasswordWritter.Close();
                        _activeUser = userName;
                        return true;
                    }
                    return false;
                }
                catch (IOException e)
                {
                    Console.WriteLine(e.Message);
                    return false;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return false;
                }
            }
            return false;
        }

        public bool TryLogIn(string userName, string password)
        {
            if (userName != "" & password != "")
            {
                try
                {
                    if (File.ReadLines(dataFile).Any(line => line.Contains(String.Format("{0}:{1}", userName.ToLower(), password))))
                    {
                        _activeUser = userName;
                        return true;
                    }
                    return false;
                }
                catch (IOException e)
                {
                    Console.WriteLine(e.Message);
                    return false;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return false;
                }
            }
            else
                return false;
        }
    }
}