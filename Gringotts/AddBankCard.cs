﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gringotts
{
    public partial class AddBankCard : Form
    {
        Gringotts GringottsUI;
        string WallettName;
        int WalletPin;
        public AddBankCard(string walletname, int walletPIN)
        {
            InitializeComponent();
            this.WallettName = walletname;
            this.WalletPin = walletPIN;
        }
        private void CardDoneButton_Click(object sender, EventArgs e)
        {
            bool valid = true;
            if (BankNameBox.Text != "")
            {
                foreach (char letter in BankNameBox.Text)
                    if (!(letter >= 'A' && letter <= 'Z' || letter >= 'a' && letter <= 'z'))
                        valid = false;
            }
            else
                valid = false;
            if (valid)
            {
                GringottsUI = new Gringotts(this.WallettName, this.WalletPin, BankNameBox.Text);
                GringottsUI.Show();
                this.Hide();
                MessageBox.Show("Success!");
            }
            else
                MessageBox.Show("Invalid Name");
        }
    }
}