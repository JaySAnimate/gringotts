﻿using System;
using System.IO;

namespace Gringotts
{
    interface IWalletActions
    {
        bool CashIn(decimal amount, out string status);
        bool Pay(out string status, bool viaCard, params UtilityServices[] services);
        bool Pay(decimal cost, string placeOrShop, string itemName, bool viaCard, out string status);
        string PrintCardStatement(out string status);
        string WalletPaymentHistory(out string status);
        bool SaveState(out string status);
    }

    struct UtilityPayments
    {
        public UtilityServices Service;
        public decimal PerUnitFee;
        public int UnitsSpent;
        public decimal Debt;
        public string UpTo;
        public decimal Paid;
        public string PaymentDate;

        public UtilityPayments(UtilityServices service)
        {
            this.Service = service;
            this.PerUnitFee = GringottsWallet.UnitFees[this.Service];
            this.UnitsSpent = GringottsWallet.UnitsSpent[this.Service];
            this.Debt = this.UnitsSpent * this.PerUnitFee;
            this.UpTo = DateTime.Now.Date.ToString();
            this.Paid = 0m;
            this.PaymentDate = "None";
        }
    }

    enum UtilityServices
    {
        None,
        Electricity,
        Water,
        Gas,
        Phone,
        TV,
        Internet
    }

    struct EntertainmentOrShopping
    {
        public string PlaceOrShop;
        public string ItemName;
        public decimal Cost;
        public string PaidDate;

        public EntertainmentOrShopping(string placeOrShop, string itemName, decimal cost)
        {
            this.PlaceOrShop = placeOrShop;
            this.ItemName = itemName;
            this.Cost = cost;
            this.PaidDate = DateTime.Now.ToString();     
        }
    }
}
