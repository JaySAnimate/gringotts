﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gringotts
{
    public partial class LogInUI : Form
    {
        private LogIn Log;
        private Gringotts Wallet;        
        private SignUpUI SignUpWindow;        
        public LogInUI()
        {
            InitializeComponent();
            Log = new LogIn();
        }

        private void LogInUI_Load(object sender, EventArgs e)
        {

        }

        private void XButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LogInButton_Click(object sender, EventArgs e)
        {
            if (Log.TryLogIn(LogInBox.Text, PasswordBox.Text))
            {
                Wallet = new Gringotts();
                Wallet.Show();
                this.Hide();
            }
            else
                MessageBox.Show("Invalid Credentials");
        }

        private void MainSignUpButton_Click(object sender, EventArgs e)
        {
            SignUpWindow = new SignUpUI(ref this.Log);
            SignUpWindow.Show();
            this.Hide();
        }
    }
}