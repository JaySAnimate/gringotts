﻿namespace Gringotts
{   
    class Wallet : Account
    {
        public bool TopUpFromBankCard(BankAccount Card, decimal amount)
        {
            this._balance += Card.TrasferMoney(amount, out bool Transferrable);
            return Transferrable;
        }
    }
}