﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gringotts
{
    public partial class SignUpUI : Form
    {
        LogIn Log;
        ConfigureWallet WalletConfig;
        public SignUpUI(ref LogIn log)
        {
            InitializeComponent();
            Log = log;
        }

        private void XButton_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void SignUpButton_Click(object sender, EventArgs e)
        {
            if (Log.TrySignUp(this.LogInBox.Text, this.PasswordBox.Text))
            {
                MessageBox.Show("Success!");
                WalletConfig = new ConfigureWallet();
                this.Hide();
                WalletConfig.Show();
            }
            else
                MessageBox.Show("Invalid Credentials.");
        }
    }
}