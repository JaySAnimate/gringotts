﻿namespace Gringotts
{
    partial class SignUpUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SignUpUI));
            this.SignUpButton = new System.Windows.Forms.Button();
            this.PasswordLabel = new System.Windows.Forms.Label();
            this.UsernameLabel = new System.Windows.Forms.Label();
            this.GringottsWallet = new System.Windows.Forms.Label();
            this.LogInBox = new System.Windows.Forms.TextBox();
            this.PasswordBox = new System.Windows.Forms.TextBox();
            this.SignUpPageLabel = new System.Windows.Forms.Label();
            this.XButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SignUpButton
            // 
            this.SignUpButton.FlatAppearance.BorderSize = 0;
            this.SignUpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SignUpButton.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SignUpButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.SignUpButton.Location = new System.Drawing.Point(51, 378);
            this.SignUpButton.Name = "SignUpButton";
            this.SignUpButton.Size = new System.Drawing.Size(300, 40);
            this.SignUpButton.TabIndex = 2;
            this.SignUpButton.Text = "Sign Up";
            this.SignUpButton.UseVisualStyleBackColor = true;
            this.SignUpButton.Click += new System.EventHandler(this.SignUpButton_Click);
            // 
            // PasswordLabel
            // 
            this.PasswordLabel.AutoSize = true;
            this.PasswordLabel.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.PasswordLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.PasswordLabel.Location = new System.Drawing.Point(48, 250);
            this.PasswordLabel.Name = "PasswordLabel";
            this.PasswordLabel.Size = new System.Drawing.Size(65, 17);
            this.PasswordLabel.TabIndex = 21;
            this.PasswordLabel.Text = "Password";
            // 
            // UsernameLabel
            // 
            this.UsernameLabel.AutoSize = true;
            this.UsernameLabel.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.UsernameLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.UsernameLabel.Location = new System.Drawing.Point(48, 179);
            this.UsernameLabel.Name = "UsernameLabel";
            this.UsernameLabel.Size = new System.Drawing.Size(68, 17);
            this.UsernameLabel.TabIndex = 20;
            this.UsernameLabel.Text = "Username";
            // 
            // GringottsWallet
            // 
            this.GringottsWallet.AutoSize = true;
            this.GringottsWallet.Font = new System.Drawing.Font("Calibri", 20F, System.Drawing.FontStyle.Bold);
            this.GringottsWallet.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.GringottsWallet.Location = new System.Drawing.Point(102, 97);
            this.GringottsWallet.Name = "GringottsWallet";
            this.GringottsWallet.Size = new System.Drawing.Size(199, 33);
            this.GringottsWallet.TabIndex = 19;
            this.GringottsWallet.Text = "Gringotts Wallet";
            // 
            // LogInBox
            // 
            this.LogInBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(83)))), ((int)(((byte)(96)))));
            this.LogInBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LogInBox.Font = new System.Drawing.Font("Calibri", 15F);
            this.LogInBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.LogInBox.Location = new System.Drawing.Point(51, 199);
            this.LogInBox.Name = "LogInBox";
            this.LogInBox.Size = new System.Drawing.Size(300, 32);
            this.LogInBox.TabIndex = 0;
            this.LogInBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // PasswordBox
            // 
            this.PasswordBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(83)))), ((int)(((byte)(96)))));
            this.PasswordBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PasswordBox.Font = new System.Drawing.Font("Calibri", 15F);
            this.PasswordBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.PasswordBox.Location = new System.Drawing.Point(51, 270);
            this.PasswordBox.Name = "PasswordBox";
            this.PasswordBox.Size = new System.Drawing.Size(300, 32);
            this.PasswordBox.TabIndex = 1;
            this.PasswordBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // SignUpPageLabel
            // 
            this.SignUpPageLabel.AutoSize = true;
            this.SignUpPageLabel.Font = new System.Drawing.Font("Calibri", 8F, System.Drawing.FontStyle.Bold);
            this.SignUpPageLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.SignUpPageLabel.Location = new System.Drawing.Point(164, 130);
            this.SignUpPageLabel.Name = "SignUpPageLabel";
            this.SignUpPageLabel.Size = new System.Drawing.Size(65, 13);
            this.SignUpPageLabel.TabIndex = 23;
            this.SignUpPageLabel.Text = "Sign Up Page";
            // 
            // XButton
            // 
            this.XButton.FlatAppearance.BorderSize = 0;
            this.XButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.XButton.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Bold);
            this.XButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.XButton.Location = new System.Drawing.Point(353, 12);
            this.XButton.Name = "XButton";
            this.XButton.Size = new System.Drawing.Size(35, 35);
            this.XButton.TabIndex = 3;
            this.XButton.Text = " X";
            this.XButton.UseVisualStyleBackColor = true;
            this.XButton.Click += new System.EventHandler(this.XButton_Click);
            // 
            // SignUpUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(83)))), ((int)(((byte)(96)))));
            this.ClientSize = new System.Drawing.Size(400, 555);
            this.Controls.Add(this.XButton);
            this.Controls.Add(this.SignUpPageLabel);
            this.Controls.Add(this.SignUpButton);
            this.Controls.Add(this.PasswordLabel);
            this.Controls.Add(this.UsernameLabel);
            this.Controls.Add(this.GringottsWallet);
            this.Controls.Add(this.LogInBox);
            this.Controls.Add(this.PasswordBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SignUpUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SignUpUI";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SignUpButton;
        private System.Windows.Forms.Label PasswordLabel;
        private System.Windows.Forms.Label UsernameLabel;
        private System.Windows.Forms.Label GringottsWallet;
        private System.Windows.Forms.TextBox LogInBox;
        private System.Windows.Forms.TextBox PasswordBox;
        private System.Windows.Forms.Label SignUpPageLabel;
        private System.Windows.Forms.Button XButton;
    }
}