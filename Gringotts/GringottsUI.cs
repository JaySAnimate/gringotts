﻿using System;
using System.Globalization;
using System.Windows.Forms;

namespace Gringotts
{
    public partial class Gringotts : Form
    {
        GringottsWallet MainApp;
        public Gringotts()
        {
            InitializeComponent();
            
            UtilityPayPanel.Hide();
            CashInPanel.Hide();
            TopUpPanel.Hide();
            WalletUserName.Text = LogIn._activeUser;
            MainApp = new GringottsWallet();  
            try
            {
                CardBalance.Text = SetBalance(MainApp.Card.Balance);
                WalletBalance.Text = SetBalance(MainApp.Wallet.Balance);
            }
            catch(NullReferenceException n)
            {
                Console.WriteLine(n.Message);
                MessageBox.Show("Some Balance info was lost.");            
            }
        }
        public Gringotts(string walletname, int walletPIN, string bankName)
        {
            InitializeComponent();
            UtilityPayPanel.Hide();
            CashInPanel.Hide();
            TopUpPanel.Hide();
            WalletUserName.Text = LogIn._activeUser;
            MainApp = new GringottsWallet();
            if (MainApp.ConfigureServiceWallet(walletname, walletPIN, out string Status))
                MessageBox.Show("ServiceWallet is Created.");
            else
                MessageBox.Show(Status);

            if (MainApp.AddBankAccount(bankName, out string StatusBank))
                MessageBox.Show("BankCard is Added.");
            else
                MessageBox.Show(StatusBank);

            CardBalance.Text = SetBalance(MainApp.Card.Balance);
        }

        private void WaterButton_Click(object sender, EventArgs e)
        {
            Uname.Text = "Water";
            foreach (var b in MainApp.UPayments)
                if (b.Service == UtilityServices.Water)
                    UFee.Text = SetBalance(b.Debt);
            UtilityPayPanel.Show();
        }

        private void ElectricityButton_Click(object sender, EventArgs e)
        {
            Uname.Text = "Electricity";
            foreach(var b in MainApp.UPayments)
            {
                if(b.Service == UtilityServices.Electricity)
                    UFee.Text = SetBalance(b.Debt);
                Console.WriteLine(b.Service.ToString());
            }
            UtilityPayPanel.Show();
        }

        private void GasButton_Click(object sender, EventArgs e)
        {
            Uname.Text = "Gas";
            foreach (var b in MainApp.UPayments)
                if (b.Service == UtilityServices.Gas)
                    UFee.Text = SetBalance(b.Debt);
            UtilityPayPanel.Show();
        }

        private void XButton_Click(object sender, EventArgs e)
        {
            string status = "";
            try
            {
                MainApp.SaveState(out status);
                Environment.Exit(0);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Status: " + status + "\n" + ex.Message);
            }
        }

        private void MainCashIn_Click(object sender, EventArgs e)
        {
            CashInPanel.Show();
        }

        public string SetBalance(decimal bal)
        {
            return string.Format("{0:C}", bal.ToString("C", CultureInfo.CreateSpecificCulture("en-US")));
        }

        private void PhoneButton_Click(object sender, EventArgs e)
        {
            Uname.Text = "Phone";
            foreach (var b in MainApp.UPayments)
                if (b.Service == UtilityServices.Phone)
                    UFee.Text = SetBalance(b.Debt);
            UtilityPayPanel.Show();
        }

        private void InternetButton_Click(object sender, EventArgs e)
        {
            Uname.Text = "Internet";
            foreach (var b in MainApp.UPayments)
                if (b.Service == UtilityServices.Internet)
                    UFee.Text = SetBalance(b.Debt);
            UtilityPayPanel.Show();
        }

        private void TVButton_Click(object sender, EventArgs e)
        {
            Uname.Text = "TV";
            foreach (var b in MainApp.UPayments)
                if (b.Service == UtilityServices.TV)
                    UFee.Text = SetBalance(b.Debt);
            UtilityPayPanel.Show();
        }
        
        private void UPanelClose_Click(object sender, EventArgs e)
        {
            UtilityPayPanel.Hide();
        }

        private void CashInClose_Click(object sender, EventArgs e)
        {
            CashInPanel.Hide();
        }

        private void SubmitCash_Click(object sender, EventArgs e)
        {
            try
            {
                if (Decimal.Parse(CashInBox.Text) > 0)
                {
                    MainApp.Wallet.CashIn(Decimal.Parse(CashInBox.Text));
                    WalletBalance.Text = SetBalance(MainApp.Wallet.Balance);
                    CashInPanel.Hide();
                }
            }
            catch
            {
                MessageBox.Show("Invalid Amount.");
            }
        }

        private void UPayButton_Click(object sender, EventArgs e)
        {   
            for(int i = 1; i <= 6; ++i)
            {
                if (((UtilityServices)i).ToString() == Uname.Text)
                {
                    bool viaCard = viacard.Checked;
                    MainApp.Pay(out string status, viaCard, (UtilityServices)i);
                    if (viaCard)
                        CardBalance.Text = SetBalance(MainApp.Card.Balance);
                    WalletBalance.Text = SetBalance(MainApp.Wallet.Balance);
                    UtilityPayPanel.Hide();
                    MessageBox.Show(status);
                    break;
                }
            }
        }

        private void TopUpClose_Click(object sender, EventArgs e)
        {
            TopUpPanel.Hide();
        }

        private void ToWallet_Click(object sender, EventArgs e)
        {
            try
            {
                if (Decimal.Parse(ToWalletBox.Text) > 0 & Decimal.Parse(ToWalletBox.Text) < MainApp.Card.Balance)
                {
                    MainApp.Wallet.TopUpFromBankCard(MainApp.Card, Decimal.Parse(ToWalletBox.Text));
                    WalletBalance.Text = SetBalance(MainApp.Wallet.Balance);
                    CardBalance.Text = SetBalance(MainApp.Card.Balance);
                    TopUpPanel.Hide();
                }
            }
            catch
            {
                MessageBox.Show("Invalid Amount.");
            }
        }

        private void TopUpButton_Click(object sender, EventArgs e)
        {
            TopUpPanel.Show();
        }

        private void StatementButton_Click(object sender, EventArgs e)
        {
            string state = MainApp.PrintCardStatement(out string s);
            MessageBox.Show(state != "" ? state : "No Payments.");
        }

        private void HistoryButton_Click(object sender, EventArgs e)
        {
            string state = MainApp.WalletPaymentHistory(out string s);
            MessageBox.Show(state != "" ? state : "No Card Payments.");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MainApp.GenerateNewUtilityFees();
        }
    }
}
