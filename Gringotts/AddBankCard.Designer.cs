﻿namespace Gringotts
{
    partial class AddBankCard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddBankCard));
            this.AddCardLabel = new System.Windows.Forms.Label();
            this.CardDoneButton = new System.Windows.Forms.Button();
            this.BankNameLabel = new System.Windows.Forms.Label();
            this.GringottsWallet = new System.Windows.Forms.Label();
            this.BankNameBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // AddCardLabel
            // 
            this.AddCardLabel.AutoSize = true;
            this.AddCardLabel.Font = new System.Drawing.Font("Calibri", 8F, System.Drawing.FontStyle.Bold);
            this.AddCardLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.AddCardLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.AddCardLabel.Location = new System.Drawing.Point(163, 145);
            this.AddCardLabel.Name = "AddCardLabel";
            this.AddCardLabel.Size = new System.Drawing.Size(49, 13);
            this.AddCardLabel.TabIndex = 29;
            this.AddCardLabel.Text = "Add Card";
            // 
            // CardDoneButton
            // 
            this.CardDoneButton.FlatAppearance.BorderSize = 0;
            this.CardDoneButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CardDoneButton.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CardDoneButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.CardDoneButton.Location = new System.Drawing.Point(44, 325);
            this.CardDoneButton.Name = "CardDoneButton";
            this.CardDoneButton.Size = new System.Drawing.Size(300, 40);
            this.CardDoneButton.TabIndex = 28;
            this.CardDoneButton.Text = "Done";
            this.CardDoneButton.UseVisualStyleBackColor = true;
            this.CardDoneButton.Click += new System.EventHandler(this.CardDoneButton_Click);
            // 
            // BankNameLabel
            // 
            this.BankNameLabel.AutoSize = true;
            this.BankNameLabel.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold);
            this.BankNameLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.BankNameLabel.Location = new System.Drawing.Point(41, 191);
            this.BankNameLabel.Name = "BankNameLabel";
            this.BankNameLabel.Size = new System.Drawing.Size(155, 17);
            this.BankNameLabel.TabIndex = 27;
            this.BankNameLabel.Text = "Bank Name (only Letters)";
            // 
            // GringottsWallet
            // 
            this.GringottsWallet.AutoSize = true;
            this.GringottsWallet.Font = new System.Drawing.Font("Calibri", 20F, System.Drawing.FontStyle.Bold);
            this.GringottsWallet.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.GringottsWallet.Location = new System.Drawing.Point(95, 109);
            this.GringottsWallet.Name = "GringottsWallet";
            this.GringottsWallet.Size = new System.Drawing.Size(199, 33);
            this.GringottsWallet.TabIndex = 26;
            this.GringottsWallet.Text = "Gringotts Wallet";
            // 
            // BankNameBox
            // 
            this.BankNameBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(83)))), ((int)(((byte)(96)))));
            this.BankNameBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BankNameBox.Font = new System.Drawing.Font("Calibri", 15F);
            this.BankNameBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(184)))), ((int)(((byte)(33)))));
            this.BankNameBox.Location = new System.Drawing.Point(44, 211);
            this.BankNameBox.Name = "BankNameBox";
            this.BankNameBox.Size = new System.Drawing.Size(300, 32);
            this.BankNameBox.TabIndex = 25;
            this.BankNameBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // AddBankCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(83)))), ((int)(((byte)(96)))));
            this.ClientSize = new System.Drawing.Size(400, 555);
            this.Controls.Add(this.AddCardLabel);
            this.Controls.Add(this.CardDoneButton);
            this.Controls.Add(this.BankNameLabel);
            this.Controls.Add(this.GringottsWallet);
            this.Controls.Add(this.BankNameBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddBankCard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddBankCard";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label AddCardLabel;
        private System.Windows.Forms.Button CardDoneButton;
        private System.Windows.Forms.Label BankNameLabel;
        private System.Windows.Forms.Label GringottsWallet;
        private System.Windows.Forms.TextBox BankNameBox;
    }
}